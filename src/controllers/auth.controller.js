const AuthService = require('../services/auth.service');
const Joi = require('@hapi/joi');

const login = (req, res, next) => {
    const schema = Joi.object({
        email: Joi.string().required(),
        password: Joi.string().required()
    });
    const { error } = schema.validate(req.body);
    if (error) return next({
        statusCode: 400,
        payload: error.details[0]
    });
    AuthService.login(req.body)
        .then(response => res.send(response))
        .catch(error => next({
            payload: error.payload,
            statusCode: error.statusCode
        }));
}

module.exports = {
    login
}