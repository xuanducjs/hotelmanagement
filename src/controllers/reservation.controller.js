const ReservationService = require('../services/reservation.service');
const _ = require('lodash');
const Joi = require('@hapi/joi');

const list = (req, res, next) => {
    const data = { 
        isOnline,
        roomName,
        nationalId,
        status
    } = req.query;
    ReservationService.listReservations(data)
        .then(response => res.send({
            reservations: response
        }))
        .catch(error => next(error));
}

const getByReservationId = (req, res, next) => {
    const { reservationId } = req.params;
    ReservationService.getByReservationId(reservationId)
        .then(response => res.send({
            reservation: response
        }))
        .catch(error => next(error));
}

const create = (req, res, next) => {
    let customerJoi = {};
    req.body.isOnline = (req.body.isOnline === 'true'); 
    const isOnline = req.body.isOnline;
    const haveCustomer = (req.query.haveCustomer === 'true');
    if (!isOnline && !haveCustomer) {
        customerJoi = {
            firstName: Joi.string().required(),
            lastName: Joi.string().required(),
            phoneNumber: Joi.string().required().min(10).max(11),
            dob: Joi.date().required()
        }
    }
    const schema = Joi.object({
        nationalId: Joi.string().min(9).max(12).required(),
        roomName: Joi.string().min(4).required().required(),
        startDate: Joi.date().required(),
        endDate: Joi.date().required(),
        adults: Joi.number().min(1).required(),
        children: Joi.number().min(0).required(),
        isOnline: Joi.boolean().required(),
        ...customerJoi
    });
    
    const { error } = schema.validate({ ...req.body });
    if (error) return next({
        statusCode: 400,
        payload: error.details[0]
    });
    ReservationService.create(req.body, isOnline, haveCustomer, res.user.userName)
        .then(response => res.send({
            reservation: response
        }))
        .catch(error => next(error));
}

const updateByReservationId = (req, res, next) => {
    if (_.isEqual(res.user.role, 'admin') || _.isEqual(res.user.role, 'staff')) {
        const { reservationId } = req.params;
        const schema = Joi.object({
            status: Joi.string().required()
        });
        const { error } = schema.validate({ ...req.body });
        if (error) return next({
            statusCode: 400,
            payload: error.details[0]
        });

        ReservationService.updateByReservationId(reservationId, req.body.status, res.user.userName)
            .then(response => res.send({
                reservation: response
            }))
            .catch(error => next(error));
    } else return next({
        statusCode: 401,
        payload: {
            message: 'Not authorized!'
        }
    });

    
}

module.exports = {
    list,
    getByReservationId,
    create,
    updateByReservationId
}