const CustomerService = require('../services/customer.service');
const _ = require('lodash');
const Joi = require('@hapi/joi');

const list = (req, res, next) => {
    if (_.isEqual(res.user.role, 'admin') || _.isEqual(res.user.role, 'staff')) {
        CustomerService.listCustomers()
            .then(response => res.send({
                customers: response
            }))
            .catch(error => next(error));
    } else {
        return next({
            statusCode: 401,
            payload: {
                message: 'Not authorized!'
            }
        });
    }
}

const getByNationalId = (req, res, next) => {
    if (_.isEqual(res.user.role, 'admin') || _.isEqual(res.user.role, 'staff')) {
        const { nationalId } = req.params;
        CustomerService.getByNationalId(nationalId)
            .then(response => res.send({
                customer: response
            }))
            .catch(error => next(error));
    } else {
        return next({
            statusCode: 401,
            payload: {
                message: 'Not authorized!'
            }
        });
    }
}

const create = (req, res, next) => {
    if (_.isEqual(res.user.role, 'admin') || _.isEqual(res.user.role, 'staff')) {
        const schema = Joi.object({
            firstName: Joi.string().required(),
            lastName: Joi.string().required(),
            phoneNumber: Joi.string().required().min(10).max(11),
            nationalId: Joi.string().required().min(9).max(12),
            dob: Joi.date().required()
        });
    
        const { error } = schema.validate({ ...req.body });
        if (error) return next({
            statusCode: 400,
            payload: error.details[0]
        });
        CustomerService.create(req.body)
            .then(response => res.send({
                customer: response
            }))
            .catch(error => next(error));
    } else {
        return next({
            statusCode: 401,
            payload: {
                message: 'Not authorized!'
            }
        });
    }
}

const updateByNationalId = (req, res, next) => {
    if (_.isEqual(res.user.role, 'admin') || _.isEqual(res.user.role, 'staff')) {
        const { nationalId } = req.params;
        const schema = Joi.object({
            firstName: Joi.string(),
            lastName: Joi.string(),
            phoneNumber: Joi.string().min(10).max(11),
            nationalId: Joi.string().min(9).max(12),
            dob: Joi.date()
        });
        const { error } = schema.validate({ ...req.body });
        if (error) return next({
            statusCode: 400,
            payload: error.details[0]
        });

        CustomerService.updateByNationalId(nationalId, req.body)
            .then(response => res.send({
                    customer: response
            }))
            .catch(error => next(error));
    } else {
        return next({
            statusCode: 401,
            payload: {
                message: 'Not authorized!'
            }
        });
    }
}

const deleteByNationalId = (req, res, next) => {
    if (_.isEqual(res.user.role, 'admin') || _.isEqual(res.user.role, 'staff')) {
        const { nationalId } = req.params;
        CustomerService.deleteByNationalId(nationalId)
            .then(response => res.send({
                result: response
            }))
            .catch(error => next(error));
    } else {
        return next({
            statusCode: 401,
            payload: {
                message: 'Not authorized!'
            }
        });
    }
}

module.exports = {
    list,
    getByNationalId,
    create,
    updateByNationalId,
    deleteByNationalId
}