const UserService = require('../services/user.service');
const _ = require('lodash');
const Joi = require('@hapi/joi');

const list = (req, res, next) => {
    const role = req.query.role;
    UserService.listUsers(role)
        .then(response => res.send({
            users: response
        }))
        .catch(error => next(error));
}

const getByUserName = (req, res, next) => {
    const { username } = req.params;
    UserService.getByUserName(username)
        .then(response => res.send({
            user: response
        }))
        .catch(error => next(error));
}

const create = (req, res, next) => {
    let accountRole = 'customer';
    if (res.user) {
        if (res.user.role === 'admin') accountRole = 'staff';
        else return next({
            statusCode: 401,
            payload: {
                message: 'Not authorized!'
            }
        });
    }

    const schema = Joi.object({
        firstName: Joi.string().required(),
        lastName: Joi.string().required(),
        phoneNumber: Joi.string().required().min(10).max(11),
        nationalId: Joi.string().required().min(9).max(12),
        dob: Joi.date().required(),
        email: Joi.string().email().required(),
        userName: Joi.string().required(),
        password: Joi.string().required().min(6)
    });

    const { error } = schema.validate({ ...req.body });
    if (error) return next({
        statusCode: 400,
        payload: error.details[0]
    });
    UserService.create(req.body, accountRole)
        .then(response => res.send({
            user: response
        }))
        .catch(error => next(error));
}

const updatePassword = (req, res, next) => {
    const { username } = req.params;

    if (res.user.userName !== username) return next({
        statusCode: 401,
        payload: {
            message: 'Not authorized!'
        }
    });

    const schema = Joi.object({
        oldPassword: Joi.string().required().min(6),
        newPassword: Joi.string().required().min(6)
    });
    const { error } = schema.validate({ ...req.body });
    if (error) return next({
        statusCode: 400,
        payload: error.details[0]
    });

    UserService.updatePassword(username, req.body)
        .then(response => res.send({
                user: response
        }))
        .catch(error => next(error));
}

const updateByUserName = (req, res, next) => {
    const { username } = req.params;
    if (res.user.role !== 'admin' && res.user.userName !== username) return next({
        statusCode: 401,
        payload: {
            message: 'Not authorized!'
        }
    });

    const schema = Joi.object({
        firstName: Joi.string(),
        lastName: Joi.string(),
        phoneNumber: Joi.string().min(10).max(11),
        nationalId: Joi.string().min(9).max(12),
        dob: Joi.date()
    });
    const { error } = schema.validate({ ...req.body });
    if (error) return next({
        statusCode: 400,
        payload: error.details[0]
    });

    UserService.updateByUserName(username, req.body)
        .then(response => res.send({
                user: response
        }))
        .catch(error => next(error));
}

const deleteByUserName = (req, res, next) => {
    const { username } = req.params;

    if (res.user.role !== 'admin' && res.user.userName !== username) return next({
        statusCode: 401,
        payload: {
            message: 'Not authorized!'
        }
    });
    UserService.deleteByUserName(username)
        .then(response => res.send({
            result: response
        }))
        .catch(error => next(error));
}

module.exports = {
    list,
    getByUserName,
    create,
    updatePassword,
    updateByUserName,
    deleteByUserName
}