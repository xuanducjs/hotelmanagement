const RoomService = require('../services/room.service');
const _ = require('lodash');
const Joi = require('@hapi/joi');

const list = (req, res, next) => {
    const data = {
        startDate,
        endDate,
        adults,
        children,
        getTotalFee
    } = req.query; 
    RoomService.listRooms(data)
        .then(response => res.send({
            rooms: response
        }))
        .catch(error => next(error));
}

const getByRoomName = (req, res, next) => {
    const { roomName } = req.params;
    RoomService.getByRoomName(roomName)
        .then(response => res.send({
            room: response
        }))
        .catch(error => next(error));
}

const create = (req, res, next) => {
    if (_.isEqual(res.user.role, 'admin')) {
        const schema = Joi.object({
            roomName: Joi.string().min(4).required(),
            roomStatus: Joi.string().required(),
            roomType: Joi.string().required(),
            descripttion: Joi.string().allow('', null),
            beforeRoomPrice: Joi.number().required().min(0),
            afterRoomPrice: Joi.number().min(0).allow('', null)
        });
        
        const { error } = schema.validate({ ...req.body });
        if (error) return next({
            statusCode: 400,
            payload: error.details[0]
        });
        RoomService.create(req.body, res.user.userName)
            .then(response => res.send({
                room: response
            }))
            .catch(error => next(error));
    } else {
        return next({
            statusCode: 401,
            payload: {
                message: 'Not authorized!'
            }
        });
    }
}

const updateByRoomName = (req, res, next) => {
    if (_.isEqual(res.user.role, 'admin')) {
        const { roomName } = req.params;
        const schema = Joi.object({
            roomName: Joi.string().min(4),
            roomStatus: Joi.string(),
            roomType: Joi.string(),
            descripttion: Joi.string().allow('', null),
            beforeRoomPrice: Joi.number().min(0),
            afterRoomPrice: Joi.number().min(0).allow('', null)
        });
        const { error } = schema.validate({ ...req.body });
        if (error) return next({
            statusCode: 400,
            payload: error.details[0]
        });

        RoomService.updateByRoomName(roomName, req.body, res.user.userName)
            .then(response => res.send({
                    room: response
            }))
            .catch(error => next(error));
    } else {
        return next({
            statusCode: 401,
            payload: {
                message: 'Not authorized!'
            }
        });
    }
}

const deleteByRoomName = (req, res, next) => {
    if (_.isEqual(res.user.role, 'admin')) {
        const { roomName } = req.params;
        RoomService.deleteByRoomName(roomName, res.user.userName)
            .then(response => res.send({
                result: response
            }))
            .catch(error => next(error));
    } else {
        return next({
            statusCode: 401,
            payload: {
                message: 'Not authorized!'
            }
        });
    }
}

module.exports = {
    list,
    getByRoomName,
    create,
    updateByRoomName,
    deleteByRoomName
}