const PaymentService = require('../services/payment.service');
const _ = require('lodash');
const Joi = require('@hapi/joi');

const list = (req, res, next) => {
    if (_.isEqual(res.user.role, 'admin') || _.isEqual(res.user.role, 'staff')) {
        const data = { 
            isPayment,
            reservationId
        } = req.query;
        PaymentService.listPayments(data)
            .then(response => res.send({
                payments: response
            }))
            .catch(error => next(error));
    } else return next({
        statusCode: 401,
        payload: {
            message: 'Not authorized!'
        }
    });

    
}

const getByReservationId = (req, res, next) => {
    if (_.isEqual(res.user.role, 'admin') || _.isEqual(res.user.role, 'staff')) {
        const { reservationId } = req.params;
        PaymentService.getByReservationId(reservationId)
            .then(response => res.send({
                payment: response
            }))
            .catch(error => next(error));
    } else return next({
        statusCode: 401,
        payload: {
            message: 'Not authorized!'
        }
    });
}

const create = (req, res, next) => {
    if (_.isEqual(res.user.role, 'admin') || _.isEqual(res.user.role, 'staff')) {
        const schema = Joi.object({
            reservationId: Joi.string().required()
        });
        
        const { error } = schema.validate({ ...req.body });
        if (error) return next({
            statusCode: 400,
            payload: error.details[0]
        });
        PaymentService.create(req.body, res.user.userName)
            .then(response => res.send({
                payment: response
            }))
            .catch(error => next(error));
    } else return next({
        statusCode: 401,
        payload: {
            message: 'Not authorized!'
        }
    });

    
}

const updateByReservationId = (req, res, next) => {
    if (_.isEqual(res.user.role, 'admin') || _.isEqual(res.user.role, 'staff')) {
        const { reservationId } = req.params;

        PaymentService.updateByReservationId(reservationId, res.user.userName)
            .then(response => res.send({
                payment: response
            }))
            .catch(error => next(error));
    } else return next({
        statusCode: 401,
        payload: {
            message: 'Not authorized!'
        }
    });
}

module.exports = {
    list,
    getByReservationId,
    create,
    updateByReservationId
}