const express = require('express');
const auth = require('../middlewares/auth');

const { 
    list,
    getByReservationId,
    create,
    updateByReservationId,
 } = require('../controllers/payment.controller');

const router = express.Router();

router.get('/', auth.auth(), list);

router.get('/:reservationId', auth.auth(), getByReservationId);

router.post('/', auth.auth(), create);

router.put('/:reservationId', auth.auth(), updateByReservationId);

module.exports = router;