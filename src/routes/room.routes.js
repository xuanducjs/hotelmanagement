const express = require('express');
const auth = require('../middlewares/auth');

const { 
    list,
    getByRoomName,
    create,
    updateByRoomName,
    deleteByRoomName
 } = require('../controllers/room.controller');

const router = express.Router();

router.get('/', list);

router.get('/:roomName', getByRoomName);

router.post('/', auth.auth(), create);

router.put('/:roomName', auth.auth(), updateByRoomName);

router.delete('/:roomName', auth.auth(), deleteByRoomName);

module.exports = router;