const express = require('express');
const Joi = require('@hapi/joi');
const auth = require('../middlewares/auth');

const { list,
    getByNationalId,
    create,
    updateByNationalId,
    deleteByNationalId
 } = require('../controllers/customer.controller');

const router = express.Router();

router.get('/', auth.auth(), list);

router.get('/:nationalId', auth.auth(), getByNationalId);

router.post('/', auth.auth(), create);

router.put('/:nationalId', auth.auth(), updateByNationalId);

router.delete('/:nationalId', auth.auth(), deleteByNationalId);

module.exports = router;