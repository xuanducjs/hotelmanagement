const express = require('express');
const Joi = require('@hapi/joi');
const auth = require('../middlewares/auth');

const { list,
    getByUserName,
    create,
    updatePassword,
    updateByUserName,
    deleteByUserName
 } = require('../controllers/user.controller');

const router = express.Router();

router.get('/', auth.auth(), list);

router.get('/:username', auth.auth(), getByUserName);

router.post('/', create);

router.post('/create-staff', auth.auth(), create);

router.put('/password/:username', auth.auth(), updatePassword);

router.put('/:username', auth.auth(), updateByUserName);

router.delete('/:username', auth.auth(), deleteByUserName);

module.exports = router;