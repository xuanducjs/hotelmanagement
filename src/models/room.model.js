const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const roomSchema = new Schema({
    roomName: {
        type: String,
        required: true,
    },
    descripttion: {
        type: String,
        default: ''
    },
    roomType: {
        type: String,
        required: true
    },
    roomStatus: {
        type: String,
        required: true
    },
    beforeRoomPrice: {
        type: Number,
        required: true
    },
    afterRoomPrice: {
        type: Number,
        required: true
    },
    isDeleted: {
        type: Boolean,
        default: false
    },
    createdBy: {
        type: String,
        required: true
    },
    updatedBy: {
        type: String,
        required: true
    }
}, { timestamps: { createdAt: 'createdAt' } });

const room = mongoose.model('room', roomSchema);

module.exports = room;
