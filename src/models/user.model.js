const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const userSchema = new Schema({
    firstName: {
        type: String,
        required: true
    },
    lastName: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true,
    },
    userName: {
        type: String,
        required: true,
    },
    password: {
        type: String,
        required: true,
        minlength: 6
    },
    phoneNumber: {
        type: String,
        required: true
    },
    nationalId: {
        type: String,
        required: true
    },
    dob: {
        type: Date,
        required: true,
    },
    isDeleted: {
        type: Boolean,
        default: false
    },
    role: {
        type: String,
        default: 'customer'
    }
}, { timestamps: { createdAt: 'createdAt' } });

const user = mongoose.model('user', userSchema);

module.exports = user;
