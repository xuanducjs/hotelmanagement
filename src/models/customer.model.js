const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const customerSchema = new Schema({
    firstName: {
        type: String,
        required: true
    },
    lastName: {
        type: String,
        required: true
    },
    phoneNumber: {
        type: String,
        required: true
    },
    nationalId: {
        type: String,
        required: true
    },
    dob: {
        type: Date,
        required: true,
    },
    isDeleted: {
        type: Boolean,
        default: false
    }
}, { timestamps: { createdAt: 'createdAt' } });

const customer = mongoose.model('customer', customerSchema);

module.exports = customer;
