const mongoose = require('mongoose');
const moment = require('moment');
const Schema = mongoose.Schema;

const paymentSchema = new Schema({
    reservationId: {
        type: String,
        required: true,
    },
    totalFee: {
        type: Number,
        required: true,
    },
    isPayment: {
        type: Boolean,
        default: false
    },
    paymentDate: {
        type: Date,
        default: moment()
    },
    createdBy: {
        type: String,
        required: true
    },
    updatedBy: {
        type: String,
        required: true
    }
}, { timestamps: { createdAt: 'createdAt' } });

const payment = mongoose.model('payment', paymentSchema);

module.exports = payment;
