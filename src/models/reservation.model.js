const mongoose = require('mongoose');
const moment = require('moment');
const Schema = mongoose.Schema;

const reservationSchema = new Schema({
    reservationId: {
        type: String,
        required: true
    },
    nationalId: {
        type: String,
        required: true
    },
    roomName: {
        type: String,
        required: true
    },
    reservationDate: {
        type: Date,
        default: moment()
    },
    startDate: {
        type: Date,
        required: true
    },
    endDate: {
        type: Date,
        required: true
    },
    adults: {
        type: Number,
        required: true
    },
    children: {
        type: Number,
        required: true
    },
    status: {
        type: String,
        default: 'waiting_for_confirm'
    },
    isOnline: {
        type: Boolean,
        default: false
    },
    createdBy: {
        type: String,
        required: true
    },
    updatedBy: {
        type: String,
        required: true
    }
}, { timestamps: { createdAt: 'createdAt' } });

const reservation = mongoose.model('reservation', reservationSchema);

module.exports = reservation;
