const moment = require('moment');
const User = require('../models/user.model');
const crypto = require('crypto');
const {
    ResourceNotFoundError,
    UniqueFieldError,
    InternalServerError,
    BadPasswordError
} = require('../utils/formatErrors');

const secretKey = '@hotelAPI';

const customResponse = (data) => {
    const arrayResponse = ['firstName', 'lastName', 'email', 'userName',
        'phoneNumber', 'nationalId', 'dob', 'role'];
    return arrayResponse.reduce((acc, cur) => {
        if (cur === 'dob') {
            acc[cur] = moment(data[cur]).format('DD-MM-YYYY');
        } else acc[cur] = data[cur];
        return acc;
    }, {});
}

const listUsers = async (role, reservations) => {
    try {
        let whereClause = {};
        if (role) whereClause = { role };
        if (reservations) {
            const arrayNationalId = reservations
                .map(reservation => ({ nationalId: reservation.nationalId }));
            whereClause['$or'] = [...arrayNationalId];
        } 
        const users = await User.find({ ...whereClause, isDeleted: false });
        const usersMapping = users.map(user => customResponse(user));
        return usersMapping;   
    } catch (error) {
        throw error;
    }
};

const getUser = async ({ userName, email }) => {
    try {
        const userFound = await User.findOne(
            { 
                $or: [ 
                    { email },
                    { userName }
                ],
                isDeleted: false 
            });
        return userFound ? customResponse(userFound) : userFound;   
    } catch (error) {
        throw error;
    }
};

const getByUserName = async (username) => {
    try {
        const userFound = await User.findOne({ userName: username, isDeleted: false });
        if (!userFound) return userFound;
        return customResponse(userFound);   
    } catch (error) {
        throw error;
    }
};

const getByNationalId = async (nationalId) => {
    try {
        const userFound = await User.findOne({ nationalId, isDeleted: false });
        if (!userFound) return userFound;
        return customResponse(userFound);   
    } catch (error) {
        throw error;
    }
};


const create = async (data, role = 'customer') => {
    try {
        const uniqueFields = ['email', 'userName'];
        const foundUser = await getUser(data);
        if (!foundUser) {
            const hashPassword = crypto.createHmac('sha256', secretKey).update(data.password).digest('hex');
            const user = new User({ ...data, password: hashPassword, role, dob: new Date(data.dob) });
            const result = await user.save();
            return customResponse(result);
        } else {
            const invalidUniqueField = uniqueFields.find(field => foundUser[field] === data[field]);
            throw new UniqueFieldError(invalidUniqueField);
        }   
    } catch (error) {
        throw error;
    }
};

const updatePassword = async (username, data) => {
    try {
        const foundUser = await getUser({ userName: username });
        const hashOldPassword = crypto.createHmac('sha256', secretKey).update(data.oldPassword).digest('hex');
        const passUser = await User.findOne({ password: hashOldPassword });
        if (!foundUser) {
            throw new ResourceNotFoundError('user');
        } else {
            if (!passUser) {
                throw new BadPasswordError();
            } else {
                const hashNewPassword = crypto.createHmac('sha256', secretKey).update(data.newPassword).digest('hex');
                const result = await User.updateOne({ userName: username, isDeleted: false }, { password: hashNewPassword });
                if (!result.nModified) throw new InternalServerError(500); 
                return customResponse({ ...foundUser, ...data});
            }
        }
    } catch (error) {
        throw error
    }
};

const updateByUserName = async (username, data) => {
    try {
        const foundUser = await getUser({ userName: username })
        if (!foundUser) {
            throw new ResourceNotFoundError('user');
        } else {
            const result = await User.updateOne({ userName: username, isDeleted: false }, { ...data });
            if (!result.nModified) throw new InternalServerError(500); 
            return customResponse({ ...foundUser, ...data});
        }
    } catch (error) {
        throw error
    }
};

const deleteByUserName = async (username) => {
    try {
        const foundUser = await getUser({ userName: username })
        if (!foundUser) {
            throw new ResourceNotFoundError('user');
        } else {
            const result = await User.updateOne({ userName: username, isDeleted: false }, { isDeleted: true });
            if (!result.nModified) throw new InternalServerError(500);  
            return ({
                message: 'Successful',
            });
        }   
    } catch (error) {
        throw error;
    }
};

module.exports = {
    listUsers,
    getUser,
    getByUserName,
    getByNationalId,
    create,
    updatePassword,
    updateByUserName,
    deleteByUserName
}