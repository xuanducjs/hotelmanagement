const moment = require('moment');
const Customer = require('../models/customer.model');
const {
    ResourceNotFoundError,
    UniqueFieldError,
    InternalServerError
} = require('../utils/formatErrors');

const customResponse = (data) => {
    const arrayResponse = ['firstName', 'lastName', 'phoneNumber', 'nationalId', 'dob'];
    return arrayResponse.reduce((acc, cur) => {
        if (cur === 'dob') {
            acc[cur] = moment(data[cur]).format('DD-MM-YYYY');
        } else acc[cur] = data[cur];
        return acc;
    }, {});
}

const listCustomers = async (reservations) => {
    try {
        let whereClause = {};
        if (reservations) {
            const arrayNationalId = reservations
                .map(reservation => ({ nationalId: reservation.nationalId }));
            whereClause['$or'] = [...arrayNationalId];
        } 
        const customers = await Customer.find({ ...whereClause, isDeleted:  false });
        const customersMapping = customers.map(customer => customResponse(customer));
        return customersMapping;   
    } catch (error) {
        throw error;
    }
};

const getByNationalId = async (nationalId) => {
    try {
        const customerFound = await Customer.findOne({ nationalId, isDeleted:  false });
        if (!customerFound) return customerFound;
        return customResponse(customerFound);
    } catch (error) {
        throw error
    }
};

const create = async (data) => {
    try {
        const uniqueFields = ['nationalId'];
        const foundCustomer = await getByNationalId(data.nationalId);
        if (!foundCustomer) {
            const customer = new Customer({ ...data, dob: new Date(data.dob) });
            const result = await customer.save();
            return customResponse(result);
        } else {
            const invalidUniqueField = uniqueFields.find(field => foundCustomer[field] === data[field]);
            throw new UniqueFieldError(invalidUniqueField);
        }   
    } catch (error) {
        throw error;
    }
};

const updateByNationalId = async (nationalId, data) => {
    try {
        const foundCustomer = await getByNationalId(nationalId)
        if (!foundCustomer) {
            throw new ResourceNotFoundError('customer');
        } else {
            const result = await Customer.updateOne({ nationalId, isDeleted: false }, { ...data });
            if (!result.nModified) throw new InternalServerError(500); 
            return customResponse({ ...foundCustomer, ...data});
        }
    } catch (error) {
        throw error;
    }
};

const deleteByNationalId = async (nationalId) => {
    try {
        const foundCustomer = await getByNationalId(nationalId)
        if (!foundCustomer) {
            throw new ResourceNotFoundError('customer');
        } else {
            const result = await Customer.updateOne({ nationalId, isDeleted: false }, { isDeleted: true });
            if (!result.nModified) throw new InternalServerError(500);
            return ({
                message: 'Successful',
            });
        }
    } catch (error) {
        throw error;
    }
};

module.exports = {
    listCustomers,
    getByNationalId,
    create,
    updateByNationalId,
    deleteByNationalId
}