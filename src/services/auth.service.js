const User = require('../models/user.model');
const moment = require('moment');
const crypto = require('crypto');
const jwt = require('../utils/jwt');
const {
    BadRequestError
} = require('../utils/formatErrors');

const secretKey = '@hotelAPI';

const customResponse = (data) => {
    const arrayResponse = ['firstName', 'lastName', 'email', 'userName',
        'phoneNumber', 'nationalId', 'dob', 'role'];
    return arrayResponse.reduce((acc, cur) => {
        if (cur === 'dob') {
            acc[cur] = moment(data[cur]).format('DD-MM-YYYY');
        } else acc[cur] = data[cur];
        return acc;
    }, {});
}

const login = async ({ email, password }) => {
    const hashPassword = crypto.createHmac('sha256', secretKey).update(password).digest('hex');
    const foundUser = await User.findOne({
        email,
        password: hashPassword,
        isDeleted: false
    })
    if (foundUser) {
        const dataToken = {
            email: foundUser.email,
            role: foundUser.role,
            userName: foundUser.userName
        }
        return new Promise((resolve, reject) => jwt.sign(dataToken, (err, token) => {
            if (err) {
                return reject({
                    statusCode: 400,
                    message: err.message
                });
            }
            return resolve({
                token: token,
                user: customResponse(foundUser)
            });
            
        }));
    }
    throw new BadRequestError();
}

const checkAuth = (user) => {
    return User.findOne(user)
        .then((foundUser) => {
            if(foundUser){
                return Promise.resolve(foundUser);
            }else{
                return Promise.reject({
                    message: 'Not found user!'
                });
            }
        })
        .catch((err) => {
            return Promise.reject(err);
        });
}

module.exports = {
    login: login,
    checkAuth: checkAuth
}
