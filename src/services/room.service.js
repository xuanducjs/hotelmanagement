const moment = require('moment');
const _ = require('lodash');
const Room = require('../models/room.model');
const Reservation = require('../models/reservation.model');
const {
    ResourceNotFoundError,
    UniqueFieldError,
    InternalServerError,
    BadTypeOrStatusError
} = require('../utils/formatErrors');

const uniqueFields = ['roomName'];
const roomStatuses = ['available', 'reserved', 'maintenanced'];
const roomTypes = ['single', 'double', 'multi'];

const customResponse = (data, getTotalFee = false) => {
    const arrayResponse = ['roomName', 'roomType', 'roomStatus', 'descripttion',
        'afterRoomPrice', 'beforeRoomPrice', 'createdBy', 'updatedBy'];
    if (getTotalFee) arrayResponse.push('totalFee');
    return arrayResponse.reduce((acc, cur) => {
        acc[cur] = data[cur];
        return acc;
    }, {});
}

const listRooms = async ({ startDate, endDate, adults, children, getTotalFee }) => {
    try {
        let whereClause = {};
        if (startDate && endDate) {
            const reservationInfo = await listReservationsByDate({ endDate, startDate });
            let roomIdInfo = reservationInfo.map(reservation => reservation.roomName);
            roomIdInfo = _.uniq(roomIdInfo);
            roomIdInfo = roomIdInfo.map(room => ({ roomName: { $ne: room } }));
            if (roomIdInfo.length > 0) whereClause['$and'] = roomIdInfo;
            whereClause.roomStatus = { $ne: 'maintenanced' };
        }
        let rooms = await Room.find({ ...whereClause, isDeleted: false });
        if ((getTotalFee === 'true')) {
            const startDateValue = moment(startDate, 'YYYY-MM-DD HH:mm');
            const endDateValue = moment(endDate, 'YYYY-MM-DD HH:mm');
            const hourTime = endDateValue.diff(startDateValue, 'hour');
            rooms = rooms.map(room => ({ ...room.toJSON(), totalFee: Math.floor(room.afterRoomPrice * hourTime / 230000)*10000 }));
        }
        const roomsMapping = rooms.map(room => customResponse(room, true));
        return roomsMapping;   
    } catch (error) {
        throw error;
    }
};

const getFee = (startDate, endDate, afterRoomPrice) => {
    console.log(startDate)
    const startDateValue = moment(startDate);
    const endDateValue = moment(endDate);
    console.log(startDateValue)
    const hourTime = endDateValue.diff(startDateValue, 'hour');
    console.log(hourTime)
    console.log(Math.floor(afterRoomPrice * hourTime / 230000)*10000)
    return Math.floor(afterRoomPrice * hourTime / 230000)*10000;
}

const listReservationsByDate = async ({ startDate, endDate }) => {
    try {
        let whereClause = {};
        if (startDate && endDate) {
            startDate = moment(startDate);
            endDate = moment(endDate);
            whereClause['$or'] = [ 
                { endDate: { $gte: startDate, $lte: endDate } },
                { startDate: { $gte: startDate, $lte: endDate } },
                { startDate: { $lte: startDate }, endDate: { $gte: endDate } }
            ];
        }
        const reservations = await Reservation.find({ ...whereClause });
        return reservations;
    } catch (error) {
        throw error;
    }
};

const getByRoomName = async (roomName) => {
    try {
        const roomFound = await Room.findOne({ roomName, isDeleted: false });
        return roomFound ? customResponse(roomFound) : roomFound;   
    } catch (error) {
        throw error;
    }
};

const create = async (data, createdBy) => {
    try {
        if (!roomStatuses.includes(data.roomStatus)) throw new BadTypeOrStatusError('Room', 'status', data.roomStatus);
        if (!roomTypes.includes(data.roomType)) throw new BadTypeOrStatusError('Room', 'type', data.roomType);
        const foundRoom = await getByRoomName(data.roomName);
        if (!foundRoom) {
            const room = new Room({
                ...data,
                createdBy,
                updatedBy: createdBy,
                afterRoomPrice: data.afterRoomPrice ? parseFloat(data.afterRoomPrice ): parseFloat(data.beforeRoomPrice),
                beforeRoomPrice: parseFloat(data.beforeRoomPrice) 
            });
            const result = await room.save();
            return customResponse(result);
        } else {
            const invalidUniqueField = uniqueFields.find(field => foundRoom[field] === data[field]);
            throw new UniqueFieldError(invalidUniqueField);
        }   
    } catch (error) {
        throw error;
    }
};

const updateStatus =  async (isConfirm) => {
    try {
        let whereClause = {};
        let whereClauseRoom = {};
        let result; 
        const timeNow = moment();
        const timeFiveMinutesAgo = moment().subtract(15, 'minutes');
        if (isConfirm === 'true') {
            whereClause.startDate = {
                $gte: timeFiveMinutesAgo,
                $lte: timeNow
            };
            whereClause.status = 'waiting_for_confirm';
            result = await Reservation.find({ ...whereClause });
        } else {
            whereClause.endDate = {
                $gte: timeFiveMinutesAgo,
                $lte: timeNow
            };
            whereClause.status = 'confirmed';
            result = await Reservation.find({ ...whereClause });
        }
        const arrayRoomName = result.length > 0 ? result.map(r => ({ roomName: r.roomName })) : [];
        if (arrayRoomName.length > 0) {
            whereClauseRoom['$or'] = arrayRoomName;
            const statusBeforeUpdate = isConfirm === 'true' ? 'available' : 'reserved'; 
            const statusAfterUpdate = isConfirm === 'true' ? 'reserved' : 'available';
            await Room.updateMany(
                { ...whereClauseRoom, roomStatus: statusBeforeUpdate, isDeleted: false }, { roomStatus: statusAfterUpdate }
            );
        }
    } catch (error) {
        console.log(error)
    }
    
}

const updateByRoomName = async (roomName, data, updatedBy) => {
    try {
        if (data.roomStatus && !roomStatuses.includes(data.roomStatus)) throw new BadTypeOrStatusError('Room', 'status', data.roomStatus);
        if (data.roomType && !roomTypes.includes(data.roomType)) throw new BadTypeOrStatusError('Room', 'type', data.roomType);
        const foundRoom = await getByRoomName(roomName);
        if (!foundRoom) {
            throw new ResourceNotFoundError('room');
        } else {
            if (data.roomName) {
                const uniqueRoom = await getByRoomName(data.roomName);
                if (_.isEqual(data.roomName, uniqueRoom.roomName)) {
                    const invalidUniqueField = uniqueFields.find(field => uniqueRoom[field] === data[field]);
                    throw new UniqueFieldError(invalidUniqueField);
                }
            }
            data.afterRoomPrice = data.afterRoomPrice ? parseFloat(data.afterRoomPrice) : foundRoom.afterRoomPrice,
            data.beforeRoomPrice = data.beforeRoomPrice ? parseFloat(data.beforeRoomPrice) : foundRoom.beforeRoomPrice
            data.updatedBy = updatedBy;
            const result = await Room.updateOne({ roomName, isDeleted: false },
                { 
                    ...data,
                    afterRoomPrice:  data.afterRoomPrice,
                    beforeRoomPrice:  data.beforeRoomPrice
                }
            );
            if (!result.nModified) throw new InternalServerError(500); 
            return customResponse({ ...foundRoom, ...data});
        }
    } catch (error) {
        throw error;   
    }
};

const deleteByRoomName = async (roomName, updatedBy) => {
    try {
        const foundRoom = await getByRoomName(roomName);
        if (!foundRoom) {
            throw new ResourceNotFoundError('room');
        } else {
            const result = await Room.updateOne({ roomName, isDeleted: false }, { isDeleted: true, updatedBy });
            if (!result.nModified) throw new InternalServerError(500); 
            return ({
                message: 'Successful',
            });
        }
    } catch (error) {
        throw error;
    }
};

module.exports = {
    listRooms,
    getByRoomName,
    create,
    getFee,
    updateStatus,
    updateByRoomName,
    deleteByRoomName
}