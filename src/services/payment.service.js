const moment = require('moment');
const _ = require('lodash');
const Payment = require('../models/payment.model');
const ReservationService = require('../services/reservation.service');
const {
    ResourceNotFoundError,
    InternalServerError
} = require('../utils/formatErrors');

const uniqueFields = ['reservationId'];

const customResponse = (data, notFormat) => {
    let arrayResponse = ['reservation', 'paymentDate', 'isPayment', 'totalFee', 'createdBy', 'updatedBy'];
    if (notFormat) {
        arrayResponse = ['reservationId', 'paymentDate', 'isPayment', 'totalFee', 'createdBy', 'updatedBy'];
    }
    return arrayResponse.reduce((acc, cur) => {
        if (cur === 'paymentDate') {
            acc[cur] = moment(data[cur]).format('DD-MM-YYYY HH:mm');
        } else acc[cur] = data[cur];
        return acc;
    }, {});
}

const listPayments = async ({ isPayment, reservationId }) => {
    try {
        let whereClause = {};
        if (isPayment !== undefined) whereClause.isPayment = (isPayment === 'true');
        if (reservationId) whereClause.reservationId = reservationId;
        const payments = await Payment.find({ ...whereClause });
        const paymentsMapping = payments.map(payment => customResponse(payment, true));
        return paymentsMapping; 
    } catch (error) {
        throw error;
    }
};

const getByReservationId = async (reservationId) => {
    try {
        const foundPayment = await Payment.findOne({ reservationId });
        if (!foundPayment) throw new ResourceNotFoundError('payment');
        const reservationInfo = await ReservationService.getByReservationId(reservationId);
        if (!reservationInfo) throw new ResourceNotFoundError('reservation');
        foundPayment.reservation = reservationInfo;
        return customResponse(foundPayment);
    } catch (error) {
        throw error;
    }
};

const create = async (data, createdBy) => {
    try {
        const reservationInfo = await ReservationService.getByReservationId(data.reservationId);
        if (!reservationInfo) throw new ResourceNotFoundError('reservation');
        const foundPayment = await Payment.findOne({ reservationId: data.reservationId });
        if (!foundPayment) {
            const startDate = moment(reservationInfo.startDate, 'DD-MM-YYYY HH:mm');
            const endDate = moment(reservationInfo.endDate, 'DD-MM-YYYY HH:mm');
            const afterRoomPrice = _.get(reservationInfo, 'room.afterRoomPrice');
            const hourTime = endDate.diff(startDate, 'hour');
            const totalFee = Math.floor(afterRoomPrice * hourTime / 230000)*10000;
            const payment = new Payment({ ...data, createdBy, updatedBy: createdBy, totalFee });
            let result = payment.save();
            result = (await result).toJSON();
            result.reservation = reservationInfo;
            return customResponse(result);
        } else {
            foundPayment.reservation = reservationInfo;
            return customResponse(foundPayment);
        }
    } catch (error) {
        throw error;
    }
};

const updateByReservationId = async (reservationId, updatedBy) => {
    try {
        let foundPayment = await getByReservationId(reservationId);
        if (!foundPayment) {
            throw new ResourceNotFoundError('payment');
        } else {
            const result = await Payment.updateOne({ reservationId }, { isPayment: true, updatedBy });
            if (!result.nModified) throw new InternalServerError(500);
            ReservationService.updateByReservationId(reservationId, 'paymented', updatedBy);
            foundPayment.paymentDate = moment(foundPayment.paymentDate, 'DD-MM-YYYY HH:mm');
            foundPayment.reservation.status = 'paymented';
            return customResponse({ ...foundPayment, isPayment: true, updatedBy });
        }
    } catch (error) {
        throw error
    }
};

module.exports = {
    listPayments,
    getByReservationId,
    create,
    updateByReservationId
}