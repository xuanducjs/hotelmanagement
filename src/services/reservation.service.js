const moment = require('moment');
const _ = require('lodash');
const Reservation = require('../models/reservation.model');
const UserService = require('../services/user.service');
const RoomService = require('../services/room.service');
const CustomerService = require('../services/customer.service');
const {
    ResourceNotFoundError,
    InternalServerError,
    BadReservationDateError,
    BadStartAndEndDateError,
    BadTypeOrStatusError
} = require('../utils/formatErrors');

const reservationStatuses = ['waiting_for_confirm', 'confirmed', 'paymented', 'canceled'];

const customResponse = (data) => {
    const arrayResponse = ['reservationId', 'reservationDate', 'startDate', 'endDate',
        'adults', 'children', 'status', 'isOnline', 'room', 'customer', 'createdBy', 'updatedBy'];
    return arrayResponse.reduce((acc, cur) => {
        if (cur === 'startDate' || cur === 'endDate' || cur === 'reservationDate') {
            acc[cur] = moment(data[cur]).format('DD-MM-YYYY HH:mm');
        } else acc[cur] = data[cur];
        return acc;
    }, {});
}

const listReservations = async ({ isOnline, roomName, nationalId, status }) => {
    try {
        let whereClause = {};
        if (isOnline !== undefined) whereClause.isOnline = (isOnline === 'true');
        if (roomName) whereClause.roomName = roomName;
        if (nationalId) whereClause.nationalId = nationalId;
        if (status) whereClause.status = status;
        const reservations = await Reservation.find({ ...whereClause });
        const foundRoom = await RoomService.listRooms({});
        const roomInfo = foundRoom.reduce((obj, cur) => {
                obj[cur.roomName] = cur;
                return obj;
            }, {});
        const arrayUserId = reservations.filter(reservation => {
            if (reservation.isOnline) return reservation;
        });
        const arrayCustomerId = reservations.filter(reservation => {
            if (!reservation.isOnline) return reservation; 
        });
        let reservationsOfflineMapping, reservationsOnlineMapping;
        if (arrayCustomerId.length > 0) {
            const customerOfflineInfo = await getCustomerInfos(arrayCustomerId, false);
            reservationsOfflineMapping = arrayCustomerId.map(reservation => {
                reservation.customer = customerOfflineInfo[reservation.nationalId];
                reservation.room = roomInfo[reservation.roomName];
                roomInfo[reservation.roomName].totalFee = RoomService.getFee(
                    reservation.startDate, reservation.endDate, roomInfo[reservation.roomName].afterRoomPrice
                );
                return customResponse(reservation); 
            });
        }
        if (arrayUserId.length > 0) {
            const customerOnlineInfo = await getCustomerInfos(arrayUserId, true);
            reservationsOnlineMapping = arrayUserId.map(reservation => {
                reservation.customer = customerOnlineInfo[reservation.nationalId];
                roomInfo[reservation.roomName].totalFee = RoomService.getFee(
                    reservation.startDate, reservation.endDate, roomInfo[reservation.roomName].afterRoomPrice
                );
                reservation.room = roomInfo[reservation.roomName];
                console.log(roomInfo)
                return customResponse(reservation); 
            });
        }
        return { 
            online: reservationsOnlineMapping,
            offline: reservationsOfflineMapping,
            countOnline: reservationsOnlineMapping ? reservationsOnlineMapping.length : 0,
            countOffline: reservationsOfflineMapping ? reservationsOfflineMapping.length : 0
        };   
    } catch (error) {
        throw error;
    }
};

const getCustomerInfos = async (reservations, isOnline) => {
    try {
        let customer;
        if (isOnline) {
            customer = await UserService.listUsers(null, reservations);
        } else {
            customer = await CustomerService.listCustomers(null, reservations)
        }
        return customer.reduce((obj, cur) => {
            obj[cur.nationalId] = cur;
            return obj;
        }, {});
    } catch (error) {
        throw error;
    }
};

const getByReservationId = async (reservationId) => {
    try {
        const foundReservation = await Reservation.findOne({ reservationId });
        if (!foundReservation) return foundReservation;
        let customerInfo;
        const roomInfo = await RoomService.getByRoomName(foundReservation.roomName);
        if (foundReservation.isOnline) {
            customerInfo = await UserService.getByNationalId(foundReservation.nationalId);
        } else customerInfo = await CustomerService.getByNationalId(foundReservation.nationalId);
        foundReservation.customer = customerInfo;
        foundReservation.room = roomInfo;
        return customResponse(foundReservation);  
    } catch (error) {
        throw error;
    }
};

const create = async (data, isOnline, haveCustomer, createdBy) => {
    try {
        const reservationId = moment.now();
        const startDate = moment(data.startDate);
        const endDate = moment(data.endDate);
        if (endDate.diff(startDate, 'hour') < 23 || startDate.diff(moment(), 'minute') < 0) 
            throw new BadStartAndEndDateError(moment().format('DD-MM-YYYY HH:mm'));
        const foundRoom = await RoomService.getByRoomName(data.roomName);
        if (!foundRoom) throw new ResourceNotFoundError('room');
        const foundReservation = await Reservation.findOne({
            $or: [ 
                { endDate: { $gte: startDate, $lte: endDate } },
                { startDate: { $gte: startDate, $lte: endDate } },
                { startDate: { $lte: startDate }, endDate: { $gte: endDate } }
            ],
            roomName: foundRoom.roomName
        });
        if (!foundReservation) {
            if (isOnline) {
                const foundUser = await UserService.getByNationalId(data.nationalId);
                if (!foundUser) throw new ResourceNotFoundError('user');
                const reservation = new Reservation({
                    ...data,
                    startDate,
                    endDate,
                    reservationId,
                    createdBy,
                    updatedBy: createdBy
                });
                const result = await reservation.save();
                result.customer = foundUser;
                result.room = foundRoom;
                return customResponse(result);  
            }
            if (!isOnline) {
                let reservation, result, foundCustomer, createCustomer;
                if (haveCustomer) {
                    foundCustomer = await CustomerService.getByNationalId(data.nationalId);
                    if (!foundCustomer) throw new ResourceNotFoundError('customer');
                    reservation = new Reservation({
                        ...data,
                        status: 'confirmed',
                        startDate,
                        endDate,
                        reservationId,
                        createdBy,
                        updatedBy: createdBy
                    });
                    result = await reservation.save();
                } else {
                    const userInfo = {
                        firstName: data.firstName,
                        lastName: data.lastName,
                        phoneNumber: data.phoneNumber,
                        nationalId: data.nationalId,
                        dob: data.dob
                    };
                    const reservationInfo = {
                        roomName: data.roomName,
                        nationalId: data.nationalId,
                        adults: data.adults,
                        children: data.children,
                        isOnline: data.isOnline,
                        status: 'confirmed'
                    };
                    createCustomer = await CustomerService.create(userInfo);
                    reservation = new Reservation({
                        ...reservationInfo,
                        status: 'confirmed',
                        startDate,
                        endDate,
                        reservationId,
                        createdBy,
                        updatedBy: createdBy
                    });
                    result = await reservation.save();
                }
                if (_.isEqual(result.status, 'confirmed')) {
                    const resultRoom = await RoomService.updateByRoomName(foundRoom.roomName, { roomStatus: 'reserved' });
                    if (resultRoom) foundRoom.roomStatus = 'reserved';
                }
                result.customer = haveCustomer ? foundCustomer : createCustomer;
                result.room = foundRoom;
                return customResponse(result);  
            }
        } else {
            throw new BadReservationDateError(
                foundRoom.roomName,
                moment(foundReservation.startDate).format('DD-MM-YYYY HH:mm'),
                moment(foundReservation.endDate).format('DD-MM-YYYY HH:mm')
            );
        }   
    } catch (error) {
        throw error;
    }
};

const updateByReservationId = async (reservationId, status, updatedBy) => {
    if (!reservationStatuses.includes(status))
            throw new BadTypeOrStatusError('Reservation', 'status', status);
    try {
        let foundReservation = await Reservation.findOne({ reservationId });
        if (!foundReservation) {
            throw new ResourceNotFoundError('reservation');
        } else {
            const roomInfo = await RoomService.getByRoomName(foundReservation.roomName);
            let customerInfo;
            if (foundReservation.isOnline) {
                customerInfo = await UserService.getByNationalId(foundReservation.nationalId);
            } else {
                customerInfo = await CustomerService.getByNationalId(foundReservation.nationalId);
            }
            const result = await Reservation.updateOne({ reservationId }, { status, updatedBy });
            if (!result.nModified) throw new InternalServerError(500);
            if (_.isEqual(status, 'confirmed')) {
                const resultRoom = await RoomService.updateByRoomName(roomInfo.roomName, { roomStatus: 'reserved' });
                if (resultRoom) roomInfo.roomStatus = 'reserved';
            }
            if (_.isEqual(status, 'paymented')) {
                const resultRoom = await RoomService.updateByRoomName(roomInfo.roomName, { roomStatus: 'available' });
                if (resultRoom) roomInfo.roomStatus = 'available';
            }
            foundReservation = foundReservation.toJSON();
            foundReservation.customer = customerInfo;
            foundReservation.room = roomInfo;
            return customResponse({ ...foundReservation, status, updatedBy });
        }
    } catch (error) {
        throw error
    }
};

module.exports = {
    listReservations,
    getByReservationId,
    create,
    updateByReservationId
}