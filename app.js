const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const RoomService = require('./src/services/room.service');
const CronJob = require('cron').CronJob;

const adminUtils = require('./src/utils/createAdmin');

const errorHandler = require('./src/middlewares/error.handler');

const userRoutes = require('./src/routes/user.routes');
const customerRoutes = require('./src/routes/customer.routes');
const authRoutes = require('./src/routes/auth.routes');
const roomRoutes = require('./src/routes/room.routes');
const reservationRoutes = require('./src/routes/reservation.routes');
const paymentRoutes = require('./src/routes/payment.routes');

const app = express();
const port = process.env.PORT || 8081;
const cloudDB = 'mongodb+srv://admin:xTELJTLK7AZEkLUt@hotelmanagement-krkfq.azure.mongodb.net/<dbname>?retryWrites=true&w=majority';
const localDB = 'mongodb://localhost:27017/HotelManagement'

const allowCrossDomain = function(req, res, next){
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type, x-access-token');
    next();
};

app.use(allowCrossDomain);
app.use(express.static('public'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use('/api/user', userRoutes);
app.use('/api/customer', customerRoutes);
app.use('/api/login', authRoutes);
app.use('/api/room', roomRoutes);
app.use('/api/reservation', reservationRoutes);
app.use('/api/payment', paymentRoutes);

app.get('/', (req, res) => {
    res.send('hi')
})

mongoose.connect(localDB, { 
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useCreateIndex: true
    }, err => {
        if (err) {
            console.log('Not connect to the database!');
        } else {
            console.log('Successfully connected to MongoDB')
        }
});

app.use(errorHandler.errorHandler());

adminUtils.createAdmin();

const job = new CronJob(
    '0 */4 * * * *',
    function () {
        console.log('============ RUNNING UPDATE ============')
        RoomService.updateStatus('true');
        RoomService.updateStatus('false');
    }
);

job.start();

app.listen(port, () => console.log(`Server is listening on port ${port}...`));
