const _ = require('lodash');
const moment = require('moment');

// dữ liệu đầu vào để map
// input 1
const inputData1 = {
    name: 'Xuan Duc',
    age: 22,
    address: 'Binh Duong',
    goByBike: true,
    haveGirlFriend: false,
    skinColor: 'Black',
    hobbies: {
        sports: {
            hands: 'Basketball',
            feet: 'football'
        },
        musicalInstrument: ['guitar', 'cajon']
    }
};
// input 2
const inputData2 = {
    ten: 'Xuan Duc',
    tuoi: 22,
    diaChi: 'Binh Duong',
    diXe: {
        xeMay: true
    },
    coBanGai: false,
    mauDa: 'Black',
    theThaoChan: 'soccer',
    theThaoTay: 'volleyball',
    soThich: {
        nhacCu: {
            nhacCuChinh: ['guitar', 'cajon']
        }
    }
};

// array các cặp key map với nhau
const arrayMappingKeys = [
    ['name', 'ten'],
    ['age', 'tuoi'],
    ['address', 'diaChi'],
    ['goByBike', 'diXe.xeMay'],
    ['haveGirlFriend', 'coBanGai'],
    ['skinColor', 'mauDa'],
    ['hobbies.sports.hands', 'theThaoTay'],
    ['hobbies.sports.feet', 'theThaoChan'],
    ['hobbies.musicalInstrument', 'soThich.nhacCu.nhacCuChinh']
];

// hàm mapping ở đây nha anh
const mappingFunction = (inputData, arrayMappingKeys, isMappingLeftToRight = true) => {
    const index = {
        input: 0,
        output: 1
    };
    if (!isMappingLeftToRight) {
        index.input = 1;
        index.output = 0;
    }
    return arrayMappingKeys.reduce((obj, currentKeyPair) => {
        const objTemp = {};
        _.set(objTemp, currentKeyPair[index.output], _.get(inputData, currentKeyPair[index.input]));
        return _.merge(obj, objTemp);
    }, {});
}

// const result = mappingFunction(inputData1, arrayMappingKeys);
// const result = mappingFunction(inputData2, arrayMappingKeys, false);
// console.log('========result', result);

const time1 = moment('04-08-2020 09:27', ('DD-MM-YYYY HH:mm'));
const time2 = moment('05-08-2020 13:25', ('DD-MM-YYYY HH:mm'));
const time = time2.diff(time1, 'hour');
// const hours = parseInt((time/(1000*60*60))%24);
const a = moment().subtract(5, 'minutes');
const b = moment();
// console.log(time1, time2, time);
// console.log(new Date(a.add(1, 'day').format('YYYY-MM-DD')))
console.log(a);
console.log(b)
console.log(moment());
